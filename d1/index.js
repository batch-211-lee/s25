console.log("Hello World");

//JSON Objects
/*
	-JSON stands for Javascript Object Notation
	-used in other programming languages
	-JS objects are not to be confused with JSON
	-Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
	-it uses double quotes for property names

	Syntax:
		{
			"propetyA" : "valueA",
			"propertyB" : "valueB"
		}
*/
/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/

//JSON Arrays
/*
	"cities" : [
		{"city" : "Quezon City", "province" : "Metro Manila", "country" : "Philippines"},
		{"city" : "Manila City", "province" : "Metro Manila", "country" : "Philippines"},
		{"city" : "Makati City", "province" : "Metro Manila", "country" : "Philippines"}
	]
*/

//JSON Methods
//JSON objects contains methods for parsing and converting data into strignified JSON

//Converting Data into Stringified JSON
/*
	-Stringified JSON is a javascript object converted into a string to be used in other functions of Javascript application
*/

let batchesArr = [{batchName : "BatchX"},{batchname: "BatchY"}]

//stringified method is used to convert JS objects into a string
console.log("Result from stringify method: ");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name : "Jon",
	age: 31,
	address : {
		city : "Manila",
		country: "Philippines"
	}
});

console.log(data);

//Using stringify method with variables
/*
	-When an information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	-this is commonly used when the information to be stored and sent to a backend application will come from a frontend application

	Syntax:
		JSON.stringify({
			propertyA : variableA,
			propertyB : variableB
		});
*/

// let firstName = prompt("What is your first name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("What is your age?");
// let address = {
// 	city : prompt("Which city do you live in?"),
// 	country : prompt("Which country do you live in?")
// };

// let otherData = JSON.stringify({
// 	firstName : firstName,
// 	lastName : lastName,
// 	age : age,
// 	address : address
// })

//console.log(otherData);

//Converting stringified JSON into Javascript objects
/*
	- objects are common data types used in applications because of complex data structures that can be created out of them
	-Information is commonly sent to applications in stringified JSON and then converted back into objects
	- This happens both for sending information to backend application and vice versa
*/

let batchesJSON = `[
{"batchName" : "Batch X"}, 
{"batchName" : "Batch Y"}
]`;
console.log("Resulty from parse method");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{"name" : "John", "Age" : "31", "address" : { "city" : "Manila", "country" : "Philippines"}}`

console.log(JSON.parse(stringifiedObject));

//parse turns into object
//stringified turns to string